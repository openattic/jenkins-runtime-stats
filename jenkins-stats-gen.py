#!/usr/bin/env python3

"""
Jenkins statistics generator.

This script will generate a statistic about all matching builds for a specified job.

Usage:
    jenkins-stats-gen.py <jenkins_url> <job_name> [--ignore-text=<ignore_text>]
    [--format=(text|json)] [--status=(success|failure|aborted)]...

Options:
    --ignore-text=<ignore_text>         This will ignore matched tested jobs if they contain the
                                        specified string in their console output text.

    --status=(success|failure|aborted)  This will filter builds which do not have the specified
                                        status. [default: success]

    --format=(text|json)                Control the output format. [default: text]

Exit codes:
    0                                   Success
    1                                   No builds matched the criteria.
"""

import docopt
import requests
import json
import sys

args = docopt.docopt(__doc__)


def get(url, json=True):
    url = url.rstrip('/')
    if json:
        url += '/api/json'
    response = requests.get(url)
    response.raise_for_status()
    return response.json() if json else response.text


def get_job(jenkins_url, job_name):
    return get('{}/job/{}'.format(jenkins_url.rstrip('/'), job_name))


def seconds_to_str(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    if h:
        return '{} hours, {} minutes and {} seconds'.format(h, m, s)
    elif m:
        return '{} minutes and {} seconds'.format(m, s)
    else:
        return '{} seconds'.format(s)


job = get_job(args['<jenkins_url>'], args['<job_name>'])
build_urls = [build['url'] for build in job['builds']]
builds = [{'build_info': get(url), 'console_text': get(url + 'consoleText', json=False)}
          for url in build_urls]

filtered_builds = [build for build in builds if
                   build['build_info']['duration'] != 0 and
                   build['build_info']['result'].lower() in args['--status']]
if args['--ignore-text']:
    filtered_builds = [build for build in filtered_builds if
                       args['--ignore-text'] not in build['console_text']]

if len(filtered_builds) == 0:
    sys.stderr.write("No builds matched the criteria.")
    sys.exit(1)

amount_of_builds = len(filtered_builds)
durations = [int(build['build_info']['duration'] / 1000) for build in filtered_builds
             if build['build_info']['duration'] != 0]
sum_duration = sum(durations)
average = int(sum_duration/amount_of_builds)
min_duration = min(durations)
max_duration = max(durations)

if args['--format'] == 'text':
    print('The Job {} has {} (valid) history items which required a sum of {} to complete.\n'
          'The average build duration is {}. The fastest builds took {}. The slowest build took {}.'
          .format(
            args['<job_name>'],
            amount_of_builds,
            seconds_to_str(sum_duration),
            seconds_to_str(average),
            seconds_to_str(min_duration),
            seconds_to_str(max_duration)))
elif args['--format'] == 'json':
    print(json.dumps({
        'job_name': args['<job_name>'],
        'build_amount': amount_of_builds,
        'duration_sum': sum_duration,
        'average': average,
        'min': min_duration,
        'max': max_duration,
    }))
